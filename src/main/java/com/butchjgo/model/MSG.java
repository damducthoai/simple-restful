package com.butchjgo.model;

/**
 * Created by root on 7/12/2017.
 */
public class MSG {
    boolean status;
    String msg;

    public MSG(boolean status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }
}
