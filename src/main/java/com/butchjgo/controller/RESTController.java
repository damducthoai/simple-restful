package com.butchjgo.controller;

import com.butchjgo.model.MSG;
import com.butchjgo.model.Person;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by root on 7/12/2017.
 */
@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class RESTController {

    @Resource(name = "PersonList")
    private Map<Integer, Person> listPerson;
    @Resource(name = "atomicInt")
    private AtomicInteger atomicInteger;

    @PostMapping
    MSG doPost(@RequestBody Person person) {
        int id = atomicInteger.incrementAndGet();
        listPerson.put(id, person);
        return new MSG(true, "Added successfully");
    }

    @GetMapping
    Map<Integer, Person> doGet() {
        return listPerson;
    }

    @GetMapping(value = "{id}")
    Person getPerson(@PathVariable int id) {
        return listPerson.get(id);
    }

    @PutMapping(value = "{id}")
    String doUpdatePerson(@PathVariable int id, @RequestBody Person person) {
        if (listPerson.containsKey(id)) {
            listPerson.put(id, person);
            //language=JSON
            return "{\"success\":true,\"msg\":\"updated successfully\"}";
        }
        //language=JSON
        return "{\"success\":false,\"msg\":\"id not found\"}";
    }

    @DeleteMapping(value = "{id}")
    String doDelete(@PathVariable int id) {
        if (listPerson.containsKey(id)) {
            //language=JSON
            listPerson.remove(id);
            return "{\"success\":true,\"msg\":\"delete successfully\"}";
        }
        //language=JSON
        return "{\"success\":false,\"msg\":\"id not found\"}";
    }
}
